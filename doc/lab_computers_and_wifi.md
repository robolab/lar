---
title:  Lab Computers and Wifi
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2022-02-14
update: 2022-02-24, 2023-03-16
---

# Lab Computers and Wifi


## Lab Desktop computers

 - All lab computers uses Debian linux as their main operation system.
 - Credentials are same as on other linux computers at CTU.
 - Home directories are synchronized across all computer that include also robots.
 - Password for lab computers can be generated here: [https://cw.felk.cvut.cz/password/](https://cw.felk.cvut.cz/password/)

## Server turtle

 - You can use the server turtle for working on the robots similar way to Desktop computers.
 - Name of the computer is **turtle.felk.cvut.cz**. You can connect from your own notebook on eduoram.
 - Credentials are the same as on other Linux computers at CTU, and you have mounted your home directory.
 - From the Server turtle you can connect to the robots.

## Robot computers

 - System is same as on the desktops.
 - There is same system on the robot computers as on the desktops.
 - Credentials are also the same, and your home is mounted.
 - SSH is used to cennect to the robot computers, using either robot name (turle01, turtle02 ...) or their IP address (192.168.65.XX) where XX is 20 + turtleId

```sh
# from lab computer
ssh -X [username]@[turtleName]

# from personal computer connected to lab wifi
ssh -X [username]@[turtleIP]
```
 - `-X` flag enables X11 forwarding that allows to display graphical windows from the remote desktop (i.e. Turtlebot) on your screen.
 - `[username]` is school username (when connecting from lab computer this can be omitted along with `@`)
 - `[turtleName]` is the name of the robot i.e. `turtle01`, `turtle02` and so on, it can be found on display case.
 - `[turtleIP]` is the IP address of the turtlebot and can be found on the display case.
 - Note that `-X` or `-Y` will work only on linux with X11

**Note:**  It is not a good idea directly edit your code on the robot. When the connection is lost, your work will be lost. Use the robot only for running the code and do the editing on Desktops, Server, or your own notebook.

## Lab Wifi

In order to connecto to robot from your personal computer, it must be connected to the lab wifi, then you can acces turtlebots through their IP address.

 - ssid: `turtlebots`
 - pass: `TurtlesDC`

It should be noted that the bandwidth of wifi is not infinite, and is shared with robots, so it should be not overused, i.e. better to work from lab computers,
and at least limit the computers connected to the wifi to a single computer per group.


