---
title:  Remote connection
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2022-02-14
---

# Remote connection

 - For more details and also how to use remote desktop see [ARO:Remote Access](https://cw.fel.cvut.cz/wiki/courses/aro/tutorials/remote_access)

 - Connect to turtle server to get `<target-computer>`
```sh
ssh student@turtle.felk.cvut.cz # password xxx
```

 - You will be prompted to your username and password
 - Then you will be redirected to `<target-computer>` for example `e230-20`
 - As we will use X11 forwarding you should disconnect

```sh
exit
```

- And reconnect with the following command (pay attention to the flags):

```sh
ssh -Y -J <username>@turtle.felk.cvut.cz <username>@<target-computer>
```
 - You wil be asked for password, twice
 - Test X11 forwarding

```sh
gedit # xclock is not installed
```

 - Gedit windows should pop up


