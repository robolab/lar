---
title:  Connection to the robot
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2021-02-10
update: 2022-02-14
---

# Connection to the robot

 - Make sure both the robot and nuc computer are powered on
 - Wait a bit for system to boot and connect the the network
 - If using your personal computer connect it to the lab WiFi (ssid: `e210bot` password: `j6UsAC8a`), robots are only visible from local network so eduroam will not work.
 - The in terminal window connect connect to the robot using `ssh`:

```sh
ssh -X [username]@[robot name]
# or
ssh -X [username]@[robot ip]

# -X (or -Y) flag is needed to forward GUI application to the lab computer
# [username] is your school usernam
# [robot name] is robot name written on the display case
# [robot ip] is robot IP address written on the display case
```

 - The you are asked for password yous the same as for the lab computer.

