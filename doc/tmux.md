---
title: TMUX Crash Course
author: Libor Wagner
date: 2021-02-26
---


# TMUX Crash Course

 - [video: Tmux Crash Course](https://www.youtube.com/watch?v=SRkmfq6gFQs)

## Usage

Start tmux:

```
tmux
```
## Keybindings

Tmux uses Ctrl-b (`<C-b>`) as a preffix for each command. In the folowing list the `<C-b>, c` meas to press `Ctrl` and `b` together and then `c`. 

 - `<C-b>, c` create window
 - `<C-b>, x` close window
 - `<C-b>, 1` switch to window 1,2,...
 - `<C-b>, %` split window vertically into two panels
 - `<C-b>, <Left>` move focus to the left
 - `<C-b>, <Right>` move focus to the right
 - `<C-b>, x` closes a window
 - `<C-b>, :` opens command prompt
   - enter `kill-session` to close all windows, or close all one by one

## Links

 - [Basic Tmux Tutorial (video:12 min)](https://www.youtube.com/watch?v=BHhA_ZKjyxo&t=233s)
 - [A tmux crash course](https://thoughtbot.com/blog/a-tmux-crash-course)
