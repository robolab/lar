---
title:  Working with singularity container
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2021-02-10
update: 2022-02-14
---

# Using robolab singularity container on lab computer

The system which is used to work with the robots is independent from the operaing system of the lab computers and robots and is provided as an [Singularity container](https://sylabs.io/guides/3.5/user-guide/index.html). Therefore, befor using the robot you must start the container every time you wat to work with the robot.

 - Start shell in the container:

```sh
singularity shell --nv /opt/singularity/robolab/robolab_noetic_2022-03-31
# or without the --nv flag if on computer without Nvide graphics
singularity shell /opt/singularity/robolab/robolab_noetic_2022-03-31
```

# Using robolab singularity container on your computer

```sh
# Download the image
wget --no-check-certificate https://data.ciirc.cvut.cz/public/projects/teaching/lar/robolab_lar_noetic_2022.simg

# Start shell in the container
singularity shell --nv /opt/singularity/robolab/robolab_lar_noetic_2022.simg
# or without the --nv flag if on computer without Nvide graphics
singularity shell /opt/singularity/robolab/robolab_lar_noetic_2022.simg
```
 - You can also download it through browser [robolab_lar_noetic_2022.simg](https://data.ciirc.cvut.cz/public/projects/teaching/lar/robolab_lar_noetic_2022.simg) (you migh need to use `Save link as`)
