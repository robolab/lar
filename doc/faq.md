---
title: LAR Frequentli Asked Questions
author: Libor Wanger <libor.wagner@cvut.cz>
date: 2022-02-23
---


## Invalid <arg> tag: environment variable 'HOSTNAME' is not set.

```sh
echo 'export HOSTNAME' >> ~/.bashrc
```

## How to set robot password

 - You can set new password at [cw.felk.cvut.cz/password/](https://cw.felk.cvut.cz/password/)

## Cat found DISPLAY

 - This happens when you forgot the `-X` flag or on Windows without X11.
 - On windows it is possible to connect to the robot and run application that do not need graphical windows.
