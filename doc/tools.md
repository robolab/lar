---
title:  Tools
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2022-02-14
---

## Tools

In addation to the above we have created some tools which might be of some use:

[ros-tmux](scripts/ros-tmux)
  - Tool to start tmux session with prepared sourced workspaces.
  - The usage is:

```sh
ros-tmux <path-to-wokspace>
```
  - Where:
    - `<path-to-workspace>` is a path to your workspce i.e. `~/lar_ws`

[move-robot](scripts/move-robot)
  - It is a tool to move the robot at precise location, without need to restart the simultion.
  - **It is not to be used to move the robot to fulfill the exercises. Use for testing.**
  - The usage is:

```sh
move-robot <x> <y> <a>
```

  - where:
    - `<x>` and `<y>` are the new coordintes for the robot in meters.
    - `<a>` is rotation round vertival axis of the robot in radians.


[make-lar-ws](scripts/make-lar-ws)
  - Script the prepare the LAR ROS workspace at `~/lar_ws`
  - The usage is:

```sh
make-lar-ws
```

## HOW TOs

Here are some HOW-TOs that might also help you. Note that for the sake of clarity the commands to run are prepended with `$` comments with `#` and the example outputs of the command are left as they are.

 - Get transformation from base of the robot to the camera optical frame:

```sh
$ rosrun tf tf_echo /base_link /camera_depth_optical_frame

At time 0.000
- Translation: [-0.087, 0.013, 0.287]
- Rotation: in Quaternion [-0.500, 0.500, -0.500, 0.500]
            in RPY (radian) [-1.571, -0.000, -1.571]
            in RPY (degree) [-90.000, -0.000, -90.000]

# or for rgb (but in simulation optical frame for both cameras is /camera_depth_optical_frame see below)
$ rosrun tf tf_echo /base_link /camera_rgb_optical_frame

At time 0.000
- Translation: [-0.087, -0.013, 0.287]
- Rotation: in Quaternion [-0.500, 0.500, -0.500, 0.500]
            in RPY (radian) [-1.571, -0.000, -1.571]
            in RPY (degree) [-90.000, -0.000, -90.000]
```

 - Get frame name for the rgb and depth camera, see that they are the same for both cameras, this meas that these images corresponds pixel by pixel:

```sh
$ rostopic echo --noarr -n 1 /camera/rgb/image_raw

header:
  seq: 36
  stamp:
    secs: 71
    nsecs: 994000000
  frame_id: "camera_depth_optical_frame"
height: 480
width: 640
encoding: "bgr8"
is_bigendian: 0
step: 1920
data: "<array type: uint8, length: 921600>"

# or for depth camera
$ rostopic echo --noarr -n 1 /camera/depth/image_raw

header:
  seq: 19
  stamp:
    secs: 604
    nsecs: 180000000
  frame_id: "camera_depth_optical_frame"
height: 480
width: 640
encoding: "32FC1"
is_bigendian: 0
step: 2560
data: "<array type: uint8, length: 1228800>"

```

