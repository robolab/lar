---
title: Singularity Crash Course
author: Libor Wagner <libor.wagner@cvut.cz>
date: 2021-03-03
---

# Singularity Crash Course

## Installation (Ubuntu)

 - These installation steps follows the guide at [Singularity Quick Start](https://sylabs.io/guides/3.7/user-guide/quick_start.html#download-singularity-from-a-release)

 - Install dependencies, inclugin goland (which is installed from release in the Quick Start)

```sh
sudo apt-get update && sudo apt-get install -y \
    build-essential \
    libssl-dev \
    uuid-dev \
    libgpgme11-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    pkg-config \
    git \
    cryptsetup \
    golang-1.14
```

 - Download singularity release

```sh
export VERSION=3.6.4
wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-${VERSION}.tar.gz
tar -xzf singularity-${VERSION}.tar.gz
cd singularity``sh
```

 - Build and install

```sh

```

 - Installation is needed only on your computer on the lab computer singularity is already installed.
 - Other options for installation are described in [Singularity Documenetaion](https://sylabs.io/guides/3.0/user-guide/index.html)

## Usage

 - Start command line in a container

```sh
singularity shell container.sif
```

## Links

 - [Singularity Quick Start](https://sylabs.io/guides/3.0/user-guide/quick_start.html)
