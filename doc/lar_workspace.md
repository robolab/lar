---
title:  LAR Workspace Preparation
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2022-02-14
update: 2022-02-14
---

# LAR Workspace

 - **Workspace is only needed when you get to know ROS more for LAR the workspace is allready part of the singularity container**.
 - Other use of the workspace is that we can propagate changes and fixes faster than to the singularity container.
 - **Make sure you are inside the singularity container**

```sh
source /opt/ros/lar/setup.bash
```

 - Create workspace:
```sh
# create directory structure
mkdir -p ~/lar_ws/src

cd ~/lar_ws/src

# support package for this course
git clone https://gitlab.fel.cvut.cz/robolab/lar.git

# package what wraps Turtlebot control into single python class
git clone https://gitlab.fel.cvut.cz/robolab/robolab_turtlebot.git

cd ~/lar_ws

# build the workspace
catkin build
```

## Using the workspace

Every time, You wat to use the workspace you must source it, same as you had to source ROS, which is done implicitly with sourcing the workspace.

 - Source your workspace:
```sh
source ~/lar_ws/devel/setup.bash
```

 - Or use provided script to start multiple windows in [tmux](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/) as you will need at least two terminal windows (one for simulation and one for your application), the [ros-tmux](scripts/ros-tmux) will open six windows and in each will source workspace which is given as parameter:
```sh
~/lar_ws/src/lar/scripts/ros-tmux ~/lar_ws
```
 - See [tmux crash course](doc/tmux.md)


## Keep the workspace up to date

 - As there might be some changes in the lar or robolab_turtlebot repositories, it is good practice to update it from time to time.

```sh
# pull changes in lar repository
cd ~/lar_ws/src/lar
git pull

# pull changes in robolab_turtlebot repository
cd ~/lar_ws/src/robolab_turtlebot
git pull
```
