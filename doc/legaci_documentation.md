---
title:  Legaci Documentaion
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2022-02-23
---

# Legaci Documentaion


Create workspace

```sh
# enter singulatiry
singularity shell --nv /opt/singularity/robolab/robolab_noetic_2021-12-01

mkdir -p ~/lar_ws/src
cd ~/lar_ws/src

# support package for this course
git clone https://gitlab.fel.cvut.cz/robolab/lar.git

# package what wraps Turtlebot control into single python class
git clone https://gitlab.fel.cvut.cz/robolab/robolab_turtlebot.git

# package rgbd_lounch
git clone https://github.com/ros-drivers/rgbd_launch.git

cd ~/lar_ws

# build the workspace
catkin build

# source the repository
source ~/lar_ws/devel/setup.bash

#
roslaunch robolab_turtlebot bringup_realsense_D435.launch

```



