

```sh
➜ ssh student@turtle.felk.cvut.cz
student@turtle.felk.cvut.cz's password: 
Write your CVUT login please:
wagnelib
You want acces to laboratory? Select by order:
0) Lab E:130 with TurtleBots (7x HP first floor)
1) Lab E:132 (20x Fujitsu with NVIDIA GF106GL [Quadro 2000] CUDA SDK 8.0, 2.1 capability)
2) Lab E:132 (30x first floor)
3) Lab E:230 (30x second floor)
1
Recognize if e132-14 wait to connection...
Machine e132-14 is off. You want started it?
yes
Warning: Permanently added 'dhcp-rootserver,192.168.210.5' (ECDSA) to the list of known hosts.
send magic packet to 00:19:99:a2:07:bb (192.168.66.110)

Machine e132-14 started. Please wait one minute.


If you don't immediately wait to ssh connection, you may connect to e132-14
a later, by reverse ssh tunnel:

    ~$ ssh -L LOCALPORT:e132-14:PORTAPP wagnelib@turtle.felk.cvut.cz

Parameters:

    LOCALPORT - localport of your computer (more 1024)
    PORTAPP - port waiting for connect on machine e132-14

Similary you can use reverse ssh tunnel to connect on another tcp port from e132-14.
That is example for ssh connection (tunnel is run on background of your local system):

    ~$ ssh -fN -L 2222:e132-14:22 wagnelib@turtle.felk.cvut.cz
    ~$ ssh wagnelib@localhost -p 2222

Connection will continue in  0s
Connection to turtle.felk.cvut.cz closed.
```



```sh
➜ ssh -J wagnelib@turtle.felk.cvut.cz wagnelib@e132-14

wagnelib@turtle.felk.cvut.cz's password: 
channel 0: open failed: connect failed: No route to host
stdio forwarding failed
kex_exchange_identification: Connection closed by remote host
Connection closed by UNKNOWN port 65535
```
