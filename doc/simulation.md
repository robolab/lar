---
title:  Simulatioin
author: Libor Wagner <libor.wagner@cvut.cz>
date:   2022-02-24
---

# Simulatioin


## Starting simulation

- In one terminal window, start the simulation:

```sh
# without simulation gui
roslaunch lar lar_2021_simulation_bringup.launch

# with simulation gui
roslaunch lar lar_2021_simulation_bringup.launch gui:=true
```

 - The above command accepts several optional arguments:
    - `gui:=true`: (true/false) show the simulation gui (gazebo client)
    - `world_file:=track_2021.world`: one of the worlds stored in `src/lar/worlds`, you can also create you own for testing.
    - `x_pos:=0.0`: initial position of the turtlebot in meters.
    - `y_pox:=0.0`: initial position of the turtlebot in meters.
    - `yaw:=0.2`: initial rotation around vertical axis in radians.
 - Predefined worlds:
    - `Task 1:` track_2021T1_R_01.world; track_2021T1_G_01.world; track_2021T1_B_01.world
    - `Task 2 & 3:` track_2021T2_R_01.world; track_2021T3_R_01.world
    - `Other:` track_2021.world

- Then in second terminal window you can run you script to control the robot or one of the examples:


## Simulation Worlds

You can define your own worlds in `./worlds` directory. `.world` files are generated from `.xacro` files. Therefore, to create your world, copy one of the existing `.xacro` files and start from there, when done run `make` to generate the `.world` file, which is then used in the simulation by running the `roslaunch` command with your world file as explained in previous section.

[Xacro](http://wiki.ros.org/xacro) is XML macro language here used to define the [sdf](http://sdformat.org/spec) file describing the simulated environment (world). The following example shows one of the test worlds:

```xml
<?xml version='1.0'?>
<sdf version="1.4" xmlns:xacro="http://ros.org/wiki/xacro">
  <xacro:include filename="common.xacro" />
  <world name="lar_race_track_02">

    <!-- Commong definitions -->
    <xacro:common/>

    <!-- Walls -->
    <xacro:wall name="wall_x" dir="x" x="1.5" y="4" d="3" />
    <xacro:wall name="wall_y" dir="y" x="3" y="2" d="4" />
    <!-- Poles -->
    <xacro:pole name="blue_1" x="1.5" y="2.5" color="blue" />

  </world>
</sdf>
```

Split into parts, we start with a XML header, the SDF header and import of our xacro definitions [common.xacro](worlds/common.xacro), and open the world XML section:

```xml
<?xml version='1.0'?>
<sdf version="1.4" xmlns:xacro="http://ros.org/wiki/xacro">
  <xacro:include filename="common.xacro" />
  <world name="lar_race_track_02">
```

Then we call the common macro which defines the simulation properties, light and ground plane:

```xml
    <!-- Commong definitions -->
    <xacro:common/>
```

Then we define the actual track for the robot. For that we have prepared two macros:
    - `wall` which obviously define a wall with the following parameters:
        - `dir=(x/y)` the wall will be parallel with either x or y axis
        - `x=[m], y=[m]` position of the center of the wall
        - `d=[m]` length of the wall
    - `pole` is used to place track poles with the following parameters:
        - `x=[m], y=[m]` position of the pole
        - `color=(red,green,blue)` color of the pole

Other parameters of these objects are defined in `common.xacro`. So the rest of the file is just definition of two perpendicular walls and single blue pole, also the XML elements `world` and `sdf` needs to be closed:

```xml
    <!-- Walls -->
    <xacro:wall name="wall_x" dir="x" x="1.5" y="4" d="3" />
    <xacro:wall name="wall_y" dir="y" x="3" y="2" d="4" />
    <!-- Poles -->
    <xacro:pole name="blue_1" x="1.5" y="2.5" color="blue" />

  </world>
</sdf>
```

